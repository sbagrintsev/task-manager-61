package ru.tsc.bagrintsev.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.system.AboutRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class AboutListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String description() {
        return "Print about author.";
    }

    @Override
    @SneakyThrows
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        System.out.println("[CLIENT]");
        System.out.printf("Author name: %s%n", propertyService.getAuthorName());
        System.out.printf("E-mail: %s%n", propertyService.getAuthorEmail());
        System.out.println("[SERVER]");
        @NotNull final ServerAboutResponse response = systemEndpoint.getAbout(new AboutRequest());
        System.out.printf("Author name: %s%n", response.getName());
        System.out.printf("E-mail: %s%n", response.getEmail());
    }

    @EventListener(condition = "@aboutListener.name() == #consoleEvent.name")
    public void listenName(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @EventListener(condition = "@aboutListener.shortName() == #consoleEvent.name")
    public void listenShort(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String shortName() {
        return "-a";
    }

}
