package ru.tsc.bagrintsev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.system.InfoRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerInfoResponse;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class InfoListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String description() {
        return "Print system info.";
    }

    @Override
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        @Nullable final ServerInfoResponse response = systemEndpoint.getInfo(new InfoRequest());
        @NotNull final String processors = String.format("Available processors (cores): %s", response.getProcessors());
        @NotNull final String freeMemory = String.format("Free memory: %s", response.getFreeMemory());
        @NotNull final String maxMemory = String.format("Maximum memory: %s", response.getMaxMemory());
        @NotNull final String totalMemory = String.format("Total memory available to JVM: %s", response.getTotalMemory());
        @NotNull final String usedMemory = String.format("Used memory in JVM: %s", response.getUsedMemory());
        System.out.printf("%s\n%s\n%s\n%s\n%s\n", processors, maxMemory, totalMemory, freeMemory, usedMemory);
    }

    @EventListener(condition = "@infoListener.name() == #consoleEvent.name")
    public void listenName(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @EventListener(condition = "@infoListener.shortName() == #consoleEvent.name")
    public void listenShort(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String shortName() {
        return "-i";
    }

}
