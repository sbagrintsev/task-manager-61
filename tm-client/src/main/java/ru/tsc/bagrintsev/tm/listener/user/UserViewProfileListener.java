package ru.tsc.bagrintsev.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    @Override
    public String description() {
        return "View user profile.";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userViewProfileListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        @Nullable final UserDto user = authEndpoint.viewProfile(new UserViewProfileRequest(getToken())).getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("LOCKED: " + user.getLocked());
    }

    @NotNull
    @Override
    public String name() {
        return "user-view-profile";
    }

}
