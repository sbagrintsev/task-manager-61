package ru.tsc.bagrintsev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.model.IWBS;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.DateUtil;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractWBSModel extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(name = "name", nullable = false, unique = true)
    private String name = "";

    @NotNull
    @Column(name = "description", nullable = false)
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "date_created", nullable = false)
    private Date dateCreated = new Date();

    @Nullable
    @Column(name = "date_started")
    private Date dateStarted;

    @Nullable
    @Column(name = "date_finished")
    private Date dateFinished;

    @Override
    public String toString() {
        return name + " : " + description +
                "\n\tid: " + getId() +
                "\n\tstatus: " + status +
                "\tcreated: " + DateUtil.toString(dateCreated) +
                "\tstarted: " + DateUtil.toString(dateStarted) +
                "\tfinished: " + DateUtil.toString(dateFinished);
    }

}
