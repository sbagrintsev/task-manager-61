package ru.tsc.bagrintsev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "m_session")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDto extends AbstractUserOwnedDtoModel {

    @NotNull
    @Column(name = "date", nullable = false)
    private Date date = new Date();

    @Nullable
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
