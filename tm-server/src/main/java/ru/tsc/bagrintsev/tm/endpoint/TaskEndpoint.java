package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAuthDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectTaskDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserDtoService;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.dto.request.task.*;
import ru.tsc.bagrintsev.tm.dto.response.task.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;

import java.util.List;

@Controller
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskDtoService taskService;

    @NotNull
    private final IProjectTaskDtoService projectTaskService;

    public TaskEndpoint(
            @NotNull final IUserDtoService userService,
            @NotNull final IAuthDtoService authService,
            @NotNull final ITaskDtoService taskService,
            @NotNull final IProjectTaskDtoService projectTaskService
    ) {
        super(userService, authService);
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDto task = projectTaskService.bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getTaskId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskDto task = taskService.changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final TaskDto task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String sortValue = request.getSortValue();
        @Nullable final WBSSort sort = WBSSort.toSort(sortValue);
        @Nullable final List<TaskDto> tasks = taskService.findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskListByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListByProjectIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<TaskDto> tasks = taskService.findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getTaskId();
        @NotNull final TaskDto task = taskService.findOneById(userId, id);
        taskService.removeById(userId, task.getId());
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getTaskId();
        @NotNull final TaskDto task = taskService.findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDto task = projectTaskService.unbindTaskFromProject(userId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final TaskDto task = taskService.updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

}
