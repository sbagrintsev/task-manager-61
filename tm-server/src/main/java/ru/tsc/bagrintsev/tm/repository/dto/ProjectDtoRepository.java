package ru.tsc.bagrintsev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;

import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> {

    long countByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    @Transactional
    void deleteByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    boolean existsByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    @Nullable
    List<ProjectDto> findAllByUserId(@NotNull final String userId);

    @Nullable
    List<ProjectDto> findAllByUserId(
            @NotNull final String userId,
            @NotNull final Sort sort);

    @Nullable
    ProjectDto findByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value = "UPDATE ProjectDto SET name = :name, description = :description WHERE userId = :userId AND id = :id")
    void updateById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id,
            @Nullable @Param("name") final String name,
            @Nullable @Param("description") final String description);

}
