package ru.tsc.bagrintsev.tm.service.model;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.sevice.model.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.model.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.model.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.model.UserRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService extends AbstractUserOwnedService<Task> implements ITaskService {

    @NotNull
    private final TaskRepository taskRepository;

    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final UserRepository userRepository;

    @Override
    @Transactional
    public @NotNull Task add(
            @Nullable final String userId,
            @Nullable final Task task
    ) throws ModelNotFoundException, IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (task == null) throw new ModelNotFoundException();
        @Nullable final User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
        if (user == null) throw new UserNotFoundException();
        task.setUser(user);
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    public @NotNull Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, TaskNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (Status.IN_PROGRESS.equals(status)) {
            task.setStatus(status);
            task.setDateStarted(new Date());
        } else if (Status.COMPLETED.equals(status)) {
            task.setStatus(status);
            task.setDateFinished(new Date());
        } else if (Status.NOT_STARTED.equals(status)) {
            task.setStatus(status);
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void clearAll() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public @NotNull Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        add(userId, task);
        return task;
    }

    @Override
    @Transactional
    public @NotNull Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, IdIsEmptyException, NameIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return taskRepository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public @NotNull List<Task> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @Nullable final List<Task> list = taskRepository.findAllByUserId(userId);
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public @NotNull List<Task> findAll(
            @Nullable final String userId,
            @Nullable final WBSSort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        @NotNull final String order = getQueryOrder(sort);
        List<Task> list = taskRepository.findAllByUserId(userId, Sort.by(order));
        return list == null ? Collections.emptyList() : list;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        List<Task> list = taskRepository.findAllByUserIdAndProjectId(userId, projectId);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    @Transactional
    public @NotNull Task removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        taskRepository.deleteByUserIdAndId(userId, id);
        return task;
    }

    @Override
    @Transactional
    public @NotNull Collection<Task> set(@NotNull final Collection<Task> tasks) {
        if (tasks.isEmpty()) return tasks;
        return taskRepository.saveAll(tasks);
    }

    @Override
    @Transactional
    public @NotNull Task setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws TaskNotFoundException, ProjectNotFoundException {
        if (projectId != null) {
            @Nullable final Project project = projectRepository.findByUserIdAndId(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
        }
        taskRepository.setProjectId(userId, taskId, projectId);
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public long totalCount() {
        return taskRepository.count();
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        return taskRepository.countByUserId(userId);
    }

    @Override
    @Transactional
    public @NotNull Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        taskRepository.updateById(userId, id, name, description);
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

}
