package ru.tsc.bagrintsev.tm.service.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.EmailIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.user.*;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.model.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.model.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.model.UserRepository;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService extends AbstractService<User> implements IUserService {

    @Getter
    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final TaskRepository taskRepository;

    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final UserRepository userRepository;

    @NotNull
    @Override
    public User checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws PasswordIsIncorrectException, AccessDeniedException, LoginIsIncorrectException, GeneralSecurityException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final User user = findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        if (!(HashUtil.generateHash(password, user.getPasswordSalt(), iterations, keyLength)).equals(user.getPasswordHash())) {
            throw new PasswordIsIncorrectException();
        }
        return user;
    }

    @Override
    @Transactional
    public void clearAll() {
        userRepository.deleteAll();
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws GeneralSecurityException, PasswordIsIncorrectException, LoginIsIncorrectException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(password, salt, iterations, keyLength);
        @NotNull final User user = new User(login, passwordHash, salt);
        userRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws EmailIsEmptyException, UserNotFoundException {
        if (email == null || email.isEmpty()) throw new EmailIsEmptyException();
        @Nullable User user = userRepository.findFirstByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable User user = userRepository.findFirstByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public @NotNull User findOneById(@Nullable final String id) throws IdIsEmptyException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.existsByEmail(email);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.existsByLogin(login);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable final User user = userRepository.findFirstByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.save(user);
    }

    @NotNull
    @Override
    @Transactional
    public User removeByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable User user = userRepository.findFirstByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskRepository.deleteAllByUserId(userId);
        projectRepository.deleteAllByUserId(userId);
        userRepository.deleteByLogin(login);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<User> set(@NotNull Collection<User> records) {
        if (records.isEmpty()) return records;
        userRepository.saveAll(records);
        return records;
    }

    @NotNull
    @Override
    @Transactional
    public User setParameter(
            @Nullable final User user,
            @NotNull final EntityField paramName,
            @Nullable final String paramValue
    ) throws EmailAlreadyExistsException, UserNotFoundException, IncorrectParameterNameException {
        if (user == null) throw new UserNotFoundException();
        if (paramValue != null && EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue)) {
            throw new EmailAlreadyExistsException(paramValue);
        }
        try {
            switch (paramName) {
                case EMAIL:
                    user.setEmail(paramValue);
                    break;
                case FIRST_NAME:
                    user.setFirstName(paramValue);
                    break;
                case MIDDLE_NAME:
                    user.setMiddleName(paramValue);
                    break;
                case LAST_NAME:
                    user.setLastName(paramValue);
                    break;
                case LOCKED:
                    user.setLocked("true".equals(paramValue));
                    break;
                default:
                    throw new IncorrectParameterNameException(paramName, "User");
            }
            return userRepository.save(user);
        } catch (IllegalArgumentException e) {
            throw new UserNotFoundException();
        }
    }

    @NotNull
    @Override
    @Transactional
    public User setPassword(
            @Nullable final String userId,
            @Nullable final String newPassword,
            @Nullable final String oldPassword
    ) throws GeneralSecurityException, PasswordIsIncorrectException, IdIsEmptyException, AccessDeniedException, LoginIsIncorrectException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordIsIncorrectException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(newPassword, salt, iterations, keyLength);
        @NotNull User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
        checkUser(user.getLogin(), oldPassword);
        userRepository.setUserPassword(user.getLogin(), passwordHash, salt);
        return userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    @Transactional
    public User setRole(
            @Nullable final String login,
            @Nullable final Role role
    ) throws IncorrectRoleException, LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (role == null) throw new IncorrectRoleException();
        userRepository.setRole(login, role);
        @Nullable final User user = userRepository.findFirstByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public long totalCount() {
        return userRepository.count();
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable final User user = userRepository.findFirstByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.save(user);
    }

    @NotNull
    @Override
    @Transactional
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return userRepository.save(user);
    }

}
