package ru.tsc.sbagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.util.Arrays;
import java.util.List;

import static ru.tsc.bagrintsev.tm.enumerated.Status.IN_PROGRESS;
import static ru.tsc.bagrintsev.tm.enumerated.Status.NOT_STARTED;

@Category(DBCategory.class)
public final class ProjectServiceTest extends AbstractTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws AbstractException {
        projectService.clearAll();
        @NotNull final Project project = new Project();
        projectService.add(userId, project);
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertEquals(project.getId(), projectService.findAll().get(0).getId());
        @Nullable User user = projectService.findAll().get(0).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userId, user.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testAddCollection() throws IdIsEmptyException, UserNotFoundException {
        @NotNull final User user = userService.findOneById("testUserId1");
        @NotNull final Project project1 = new Project();
        project1.setUser(user);
        project1.setName("1");
        @NotNull final Project project2 = new Project();
        project2.setUser(user);
        project2.setName("2");
        @NotNull final List<Project> projectList = Arrays.asList(project1, project2);
        projectService.set(projectList);
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertEquals(2, projectService.totalCount(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void testChangeProjectStatusById() throws AbstractException {
        projectService.create(userId, "name12");
        @NotNull final String id = projectService.findAll().get(0).getId();
        Assert.assertEquals(NOT_STARTED, projectService.findOneById(userId, id).getStatus());
        Assert.assertNotNull(projectService.changeStatusById(userId, id, IN_PROGRESS));
        Assert.assertEquals(IN_PROGRESS, projectService.findOneById(userId, id).getStatus());
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectService.add(userId, project);
        projectService.add(userId, project2);
        @NotNull final Project project3 = new Project();
        project.setId("id3");
        project.setName("name3");
        projectService.add(userId2, project3);
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
        projectService.clear(userId);
        Assert.assertEquals(0, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws AbstractException {
        projectService.clearAll();
        projectService.create(userId, "name1");
        projectService.create(userId, "name2", "description2");
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertEquals("name1", projectService.findAll(userId, WBSSort.BY_NAME).get(0).getName());
        Assert.assertEquals("description2", projectService.findAll(userId, WBSSort.BY_NAME).get(1).getDescription());
        @Nullable User user = projectService.findAll().get(0).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userId, user.getId());
        user = projectService.findAll().get(1).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userId, user.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testExistsById() throws AbstractException {
        projectService.clearAll();
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectService.add(userId, project);
        Assert.assertTrue(projectService.existsById(userId, "id1"));
        Assert.assertFalse(projectService.existsById(userId, "id2"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws AbstractException {
        projectService.create(userId, "name1");
        projectService.create(userId, "name2");
        projectService.create(userId2, "name3");
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
        Assert.assertEquals(3, projectService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllSorted() throws AbstractException {
        projectService.create(userId, "name8");
        projectService.create(userId, "name6");
        projectService.create(userId2, "name3");
        projectService.create(userId2, "name1");
        Assert.assertEquals("name6", projectService.findAll(userId, WBSSort.BY_NAME).get(0).getName());
        Assert.assertEquals("name6", projectService.findAll(userId, WBSSort.BY_NAME).get(0).getName());
        Assert.assertEquals("name3", projectService.findAll(userId2, WBSSort.BY_CREATED).get(0).getName());
        Assert.assertEquals("name3", projectService.findAll(userId2, WBSSort.BY_CREATED).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectService.add(userId, project);
        projectService.add(userId, project2);
        Assert.assertEquals("name1", projectService.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", projectService.findOneById(userId, "id2").getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectService.add(userId, project);
        Assert.assertEquals("name1", projectService.findOneById(userId, "id1").getName());
        Assert.assertEquals(project.getId(), projectService.removeById(userId, "id1").getId());
        Assert.assertFalse(projectService.existsById(userId, "id1"));
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectService.add(userId, project);
        projectService.add(userId, project2);
        Assert.assertEquals(2, projectService.totalCount(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void testUpdateById() throws AbstractException {
        projectService.create(userId, "name12");
        @NotNull final String id = projectService.findAll().get(0).getId();
        Assert.assertEquals("name12", projectService.findOneById(userId, id).getName());
        Assert.assertNotNull(projectService.updateById(userId, id, "name13", "testDescription"));
        Assert.assertEquals("name13", projectService.findOneById(userId, id).getName());
        Assert.assertEquals("testDescription", projectService.findOneById(userId, id).getDescription());
    }

}
