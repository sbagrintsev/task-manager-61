package ru.tsc.sbagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;

@Category(DBCategory.class)
public final class UserServiceTest extends AbstractTest {

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws AbstractException, GeneralSecurityException {
        userService.clearAll();
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals(user.getId(), userService.findAll().get(0).getId());
    }

    @Test(expected = PasswordIsIncorrectException.class)
    @Category(DBCategory.class)
    public void testCheckUser() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
        Assert.assertNotNull(userService.checkUser("testLogin", "wrongPassword"));
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DBCategory.class)
    public void testCheckUserLocked() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
        userService.lockUserByLogin("testLogin");
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws GeneralSecurityException, AbstractException {
        userService.clearAll();
        userService.create("testLogin", "testPassword");
        userService.create("testLogin2", "testPassword2");
        Assert.assertEquals(2, userService.findAll().size());
        userService.clearAll();
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPass");
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals("testLogin", userService.findAll().get(0).getLogin());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws AbstractException, GeneralSecurityException {
        userService.clearAll();
        userService.create("testLogin", "testPass");
        userService.create("testLogin2", "testPass2");
        userService.create("testLogin3", "testPass3");
        Assert.assertEquals(3, userService.findAll().size());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testFindByEmail() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        user.setEmail("test@email.ru");
        Assert.assertEquals(user, userService.findByEmail("test@email.ru"));
        Assert.assertEquals(user, userService.findByEmail("test@email.ru"));
        Assert.assertEquals(user, userService.findByEmail("test@email.ru"));
        Assert.assertEquals(user, userService.findByEmail("wrong"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testFindByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertEquals(user.getId(), userService.findByLogin("testLogin").getId());
        Assert.assertEquals(user.getId(), userService.findByLogin("wrong").getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        @NotNull final String userId = user.getId();
        @NotNull final User user2 = userService.create("testLogin2", "testPassword2");
        @NotNull final String user2Id = user2.getId();
        Assert.assertEquals("testLogin", userService.findOneById(userId).getLogin());
        Assert.assertEquals("testLogin2", userService.findOneById(user2Id).getLogin());
    }

    @Test
    @Category(DBCategory.class)
    public void testIsEmailExists() throws AbstractException, GeneralSecurityException {
        @NotNull final User user = userService.create("testLogin", "testPassword");
        userService.setParameter(user, EntityField.EMAIL, "test@email.ru");
        Assert.assertTrue(userService.isEmailExists("test@email.ru"));
        Assert.assertFalse(userService.isEmailExists("wrong@email.ru"));
    }

    @Test
    @Category(DBCategory.class)
    public void testIsLoginExists() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPassword");
        Assert.assertTrue(userService.isLoginExists("testLogin"));
        Assert.assertFalse(userService.isLoginExists("wrongLogin"));
    }

    @Test
    @Category(DBCategory.class)
    public void testLockByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull User user = userService.create("testLogin", "testPassword");
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin("testLogin");
        user = userService.findByLogin("testLogin");
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveByLogin() throws AbstractException, GeneralSecurityException {
        userService.clearAll();
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull final User user = userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.findByLogin("testLogin"));
        Assert.assertEquals(user.getId(), userService.removeByLogin("testLogin").getId());
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test(expected = IncorrectParameterNameException.class)
    @Category(DBCategory.class)
    public void testSetParameter() throws AbstractException, GeneralSecurityException {
        @NotNull final User user1 = userService.create("testLogin", "testPassword");
        Assert.assertNull(user1.getEmail());
        userService.setParameter(user1, EntityField.EMAIL, "test@email.ru");
        Assert.assertEquals("test@email.ru", user1.getEmail());
        userService.setParameter(user1, EntityField.NAME, "error");
    }

    @Test
    @Category(DBCategory.class)
    public void testSetPassword() throws GeneralSecurityException, AbstractException {
        @NotNull User user = userService.create("testLogin", "testPassword");
        @NotNull final String userId = user.getId();
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        @Nullable final String expectedPassHash = HashUtil.generateHash("testPassword", user.getPasswordSalt(), iterations, keyLength);
        Assert.assertEquals(expectedPassHash, user.getPasswordHash());
        userService.setPassword(userId, "newPassword", "testPassword");
        user = userService.findOneById(userId);
        @Nullable final String expectedNewPassHash = HashUtil.generateHash("newPassword", user.getPasswordSalt(), iterations, keyLength);
        Assert.assertEquals(expectedNewPassHash, userService.findOneById(userId).getPasswordHash());
    }

    @Test
    @Category(DBCategory.class)
    public void testSetRole() throws AbstractException, GeneralSecurityException {
        @NotNull User user = userService.create("testLogin", "testPassword");
        Assert.assertEquals(Role.REGULAR, user.getRole());
        userService.setRole("testLogin", Role.ADMIN);
        user = userService.findByLogin("testLogin");
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws GeneralSecurityException, AbstractException {
        userService.clearAll();
        userService.create("testLogin", "testPassword");
        userService.create("testLogin2", "testPassword2");
        Assert.assertEquals(2, userService.totalCount());
    }

    @Test
    @Category(DBCategory.class)
    public void testUnlockByLogin() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPassword");
        userService.lockUserByLogin("testLogin");
        @NotNull User user = userService.findByLogin("testLogin");
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin("testLogin");
        user = userService.findByLogin("testLogin");
        Assert.assertFalse(user.getLocked());
    }

    @Test
    @Category(DBCategory.class)
    public void testUpdateUser() throws AbstractException, GeneralSecurityException {
        @NotNull User user = userService.create("testLogin", "testPassword");
        @NotNull final String userId = user.getId();
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        Assert.assertNull(user.getLastName());
        userService.updateUser(userId, "testFirst", "testLast", "testMiddle");
        user = userService.findByLogin("testLogin");
        Assert.assertEquals("testFirst", user.getFirstName());
        Assert.assertEquals("testLast", user.getLastName());
        Assert.assertEquals("testMiddle", user.getMiddleName());
    }

}
