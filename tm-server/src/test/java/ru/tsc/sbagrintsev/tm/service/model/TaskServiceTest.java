package ru.tsc.sbagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.util.Arrays;
import java.util.List;

import static ru.tsc.bagrintsev.tm.enumerated.Status.IN_PROGRESS;
import static ru.tsc.bagrintsev.tm.enumerated.Status.NOT_STARTED;

@Category(DBCategory.class)
public final class TaskServiceTest extends AbstractTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @NotNull
    private User user = new User();

    @Before
    public void setUp() throws UserNotFoundException, LoginIsIncorrectException {
        user = userService.findByLogin("test1");
        @NotNull final Project project1 = new Project();
        project1.setName("project1");
        project1.setId("project1");
        project1.setUser(user);
        @NotNull final Project project2 = new Project();
        project2.setName("project2");
        project2.setId("project2");
        project2.setUser(user);
        projectService.set(Arrays.asList(project1, project2));
    }

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws AbstractException {
        @NotNull final Task task = new Task();
        taskService.add(userId, task);
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals(task.getId(), taskService.findAll().get(0).getId());
        @Nullable final User user = taskService.findOneById(userId, task.getId()).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userId, user.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testAddCollection() throws IdIsEmptyException {
        taskService.clearAll();
        @NotNull final Task task1 = new Task();
        task1.setUser(user);
        task1.setName("1");
        @NotNull final Task task2 = new Task();
        task2.setUser(user);
        task2.setName("2");
        @NotNull final List<Task> taskList = Arrays.asList(task1, task2);
        taskService.set(taskList);
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals(2, taskService.totalCount(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void testChangeStatusById() throws AbstractException {
        taskService.create(userId, "name12");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals(NOT_STARTED, taskService.findOneById(userId, id).getStatus());
        Assert.assertNotNull(taskService.changeStatusById(userId, id, IN_PROGRESS));
        Assert.assertEquals(IN_PROGRESS, taskService.findOneById(userId, id).getStatus());
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        @NotNull final Task task3 = new Task();
        task.setId("id3");
        task.setName("name3");
        taskService.add(userId2, task3);
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws AbstractException {
        taskService.clearAll();
        taskService.create(userId, "name1");
        taskService.create(userId, "name2", "description2");
        Assert.assertEquals(2, taskService.totalCount(userId));
        Assert.assertEquals("name1", taskService.findAll(userId, WBSSort.BY_NAME).get(0).getName());
        Assert.assertEquals("description2", taskService.findAll(userId, WBSSort.BY_NAME).get(1).getDescription());
        @Nullable User user = taskService.findAll().get(0).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userId, user.getId());
        user = taskService.findAll().get(1).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(userId, user.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testExistsById() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.existsById(userId, "id1"));
        Assert.assertFalse(taskService.existsById(userId, "id2"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws AbstractException {
        taskService.create(userId, "name1");
        taskService.create(userId, "name2");
        taskService.create(userId2, "name3");
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        Assert.assertEquals(3, taskService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllByProjectId() throws AbstractException {
        @NotNull final Task task1 = new Task();
        task1.setProject(projectService.findOneById(userId, "project1"));
        task1.setName("1");
        @NotNull final Task task2 = new Task();
        task2.setProject(projectService.findOneById(userId, "project2"));
        task2.setName("2");
        taskService.add(userId, task1);
        taskService.add(userId, task2);
        Assert.assertEquals(2, taskService.totalCount(userId));
        Assert.assertEquals(1, taskService.findAllByProjectId(userId, "project1").size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllSorted() throws AbstractException {
        taskService.create(userId, "name8");
        taskService.create(userId, "name6");
        taskService.create(userId2, "name3");
        taskService.create(userId2, "name1");
        Assert.assertEquals("name6", taskService.findAll(userId, WBSSort.BY_NAME).get(0).getName());
        Assert.assertEquals("name3", taskService.findAll(userId2, WBSSort.BY_CREATED).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        Assert.assertEquals("name1", taskService.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", taskService.findOneById(userId, "id2").getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveById() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.existsById(userId, "id1"));
        Assert.assertEquals(task.getId(), taskService.removeById(userId, "id1").getId());
        Assert.assertFalse(taskService.existsById(userId, "id1"));
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        Assert.assertEquals(2, taskService.totalCount(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void testUpdateById() throws AbstractException {
        taskService.create(userId, "name12");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals("name12", taskService.findOneById(userId, id).getName());
        Assert.assertNotNull(taskService.updateById(userId, id, "name13", "testDescription"));
        Assert.assertEquals("name13", taskService.findOneById(userId, id).getName());
        Assert.assertEquals("testDescription", taskService.findOneById(userId, id).getDescription());
    }

}
